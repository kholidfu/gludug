@extends('arkitekt.master')

@section('title') About us @stop

@section('meta')
<meta name="distribution" content="Global" />
  <meta name="rating" content="General" />
  <meta name="language" content="en-us" />
  <meta name="revisit-after" content="1 days"/>
  <meta name="description" content="About {{ env('DOMAIN_NAME') }}">
  <meta name="keywords" content="">
  <meta name="robots" content="index, follow"/>
@stop

@section('content')

<div class="head-banner clearfix mb30">
    <div class="wrapper">
      <h4>Arkitekt</h4>
      <div class="site_map">
        <a href="/">Home</a>about
      </div>
      <div class="clear"></div>
    </div>
  </div>

<div class="aboutus wrapper mb30">
    <div class="dark">

      <div class="column12 biograph">
        <div class="about-title">Biography</div>  
        <div class="about-sub">company story</div>

        <p>This is <span>Photoshop's</span> version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit</p>

        <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. <span>Class aptent taciti</span> sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>

        <p>Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue.vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. </p>
      </div>

      <div class="clear"></div>

    </div>

  </div>
  <!-- end of aboutus -->

  <div class="our-team-about wrapper mb30">
      <h3 class="main-title">Meet our Team</h3>
      <span class="main-subtitle">our cool &amp; professional staff</span>

        <div class="dark">
        
            <div class="column3 team-item">

                <a href="#"><div class="team-mates">
                  <img src="/static/images/team1.jpg" alt="picture">
                </div></a>

                <div class="team-descr">
                  <h3>Mike William</h3>
                  <h5>developer</h5>
                </div>
                <div class="additionalicons">   
                  <a href="#"><i class="fa fa-google-plus"></i></a>  
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-dribbble"></i></a>
                  <a href="#"><i class="fa fa-facebook"></i></a>  
                </div>
                
            </div>
            <!-- End Slide -->
        
            <div class="column3 team-item">

                <a href="#"><div class="team-mates">
                  <img src="/static/images/team2.jpg" alt="picture">
                </div></a>

                <div class="team-descr">
                  <h3>John Doe</h3>
                  <h5>developer</h5>
                </div>
                <div class="additionalicons">   
                  <a href="#"><i class="fa fa-google-plus"></i></a>  
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-dribbble"></i></a>
                  <a href="#"><i class="fa fa-facebook"></i></a>  
                </div>
                
            </div>
            <!-- End Slide -->
        
            <div class="column3 team-item">

                <a href="#"><div class="team-mates">
                  <img src="/static/images/team3.jpg" alt="picture">
                </div></a>

                <div class="team-descr">
                  <h3>Vladimir Hannan</h3>
                  <h5>developer</h5>
                </div>
                <div class="additionalicons">   
                  <a href="#"><i class="fa fa-google-plus"></i></a>  
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-dribbble"></i></a>
                  <a href="#"><i class="fa fa-facebook"></i></a>  
                </div>
                
            </div>
            <!-- End Slide -->
        
            <div class="column3 team-item">

                <a href="#"><div class="team-mates">
                  <img src="/static/images/team4.jpg" alt="picture">
                </div></a>

                <div class="team-descr">
                  <h3>Maxi Armstrong</h3>
                  <h5>developer</h5>
                </div>
                <div class="additionalicons">   
                  <a href="#"><i class="fa fa-google-plus"></i></a>  
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-dribbble"></i></a>
                  <a href="#"><i class="fa fa-facebook"></i></a>  
                </div>

            </div>
            <!-- End Slide -->
            <div class="clear"></div>
        
        </div>
        <!-- End Slider2 -->

    </div>
    <!-- end of our-team-about -->

@stop