@extends('arkitekt.master')

@section('title'){{ env('DOMAIN_NAME') }} Categories Archive: {{ $catString }}. @stop

@section('meta')
  <meta name="distribution" content="Global" />
  <meta name="rating" content="General" />
  <meta name="language" content="en-us" />
  <meta name="revisit-after" content="1 days"/>
  <meta name="description" content="Enjoy and browse collection on Categories {{ $catString }} Source at {{ env('DOMAIN_NAME') }}">
  <meta name="keywords" content="">
  <meta name="robots" content="index, follow"/>
@stop

@section('content')

<div class="head-banner clearfix mb30">
    <div class="wrapper">
      <h4>Arkitekt</h4>
      <div class="site_map">
        <a href="/">Home</a>detail
      </div>
      <div class="clear"></div>
    </div>
  </div>

<div class="main-content wrapper dark">

      <div class="shop-content column9">
          <div class="toolbar">
            <img style="display: block; margin: 0 auto; padding: 10px;" width="728" height="90" border="0" onload="" class="img_ad" src="http://pagead2.googlesyndication.com/simgad/12743359513306449184">
          </div>

          <div class="product-grid dark">
              @foreach ($thumbs as $thumb)
              <div class="grid-item-list">
                  <div class="view view-first left-img">
                      <img src="{{ url(env('ASSET_SLUG').$thumb->walldir.'/small-'.$thumb->wallimg) }}" alt="shop1">
                  </div>
                  <div class="grid-item-text">
                    <h4><a href="{{ url(env('CATEGORY_SLUG') . $thumb->cat) }}/">{{ slugToTitle($thumb->cat) }}</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quidem vel atque nemo quo repellat consectetur non modi aperiam. Est, beatae, provident neque quas reiciendis facere expedita labore dolorum blanditiis odit voluptatem mollitia consectetur explicabo vel aliquid animi nostrum perspiciatis quo earum maiores accusamus magni quaerat minus laudantium illum nesciunt?</p>
                    <div class="end-product">
                      <div class="price">25.00</div>
                      <a href="#"><i class="fa fa-heart"></i></a>
                      <a href="#"><i class="fa fa-shopping-cart"></i></a>
                      <a href="#"><i class="fa fa-random"></i></a>
                      <a href="#" class="prod-detail">details</a>
                    </div>
                  </div>
                  <div class="clear"></div>
              </div>
              @endforeach
              <!-- End Grid List View -->
          </div>

          <div class="toolbar">
            <img style="display: block; margin: 0 auto; padding: 10px;" width="728" height="90" border="0" onload="" class="img_ad" src="http://pagead2.googlesyndication.com/simgad/12743359513306449184">
          </div>
    
          <!-- End Product Grid -->

      </div>

      <div class="shop-aside column3">


        <div class="accordion mb30">
          <h3>Categories</h3>
          <div id="accordion-container">
               <h2 class="accordion-header active-header">Watches (9)</h2> 
               <div class="accordion-content open-content" style="display: block;"> 
                   <ul>
                    <li><a href="#">• Jackets (7)</a></li>
                    <li><a href="#">• Kids &amp; Babies (5)</a></li>
                    <li><a href="#">• Electronics (4)</a></li>
                   </ul>
               </div> 
               <h2 class="accordion-header inactive-header">Jewellery (5)</h2> 
               <div class="accordion-content"> 
                   <ul>
                    <li><a href="#">• Jackets (7)</a></li>
                    <li><a href="#">• Kids &amp; Babies (5)</a></li>
                    <li><a href="#">• Electronics (4)</a></li>
                   </ul>
               </div>  
               <h2 class="accordion-header inactive-header">Technology (6)</h2> 
               <div class="accordion-content"> 
                   <ul>
                    <li><a href="#">• Jackets (7)</a></li>
                    <li><a href="#">• Kids &amp; Babies (5)</a></li>
                    <li><a href="#">• Electronics (4)</a></li>
                   </ul>
               </div> 
               <h2 class="accordion-header inactive-header">Kids &amp; Babies (8)</h2> 
               <div class="accordion-content"> 
                   <ul>
                    <li><a href="#">• Jackets (7)</a></li>
                    <li><a href="#">• Kids &amp; Babies (5)</a></li>
                    <li><a href="#">• Electronics (4)</a></li>
                   </ul>
               </div> 
               <h2 class="accordion-header inactive-header">Electronics (4)</h2> 
               <div class="accordion-content"> 
                   <ul>
                    <li><a href="#">• Jackets (7)</a></li>
                    <li><a href="#">• Kids &amp; Babies (5)</a></li>
                    <li><a href="#">• Electronics (4)</a></li>
                   </ul>
               </div> 
               <h2 class="accordion-header inactive-header">Sports (5)</h2> 
               <div class="accordion-content"> 
                   <ul>
                    <li><a href="#">• Jackets (7)</a></li>
                    <li><a href="#">• Kids &amp; Babies (5)</a></li>
                    <li><a href="#">• Electronics (4)</a></li>
                   </ul>
               </div> 
               <h2 class="accordion-header inactive-header">Bicycles (2)</h2> 
               <div class="accordion-content"> 
                   <ul>
                    <li><a href="#">• Jackets (7)</a></li>
                    <li><a href="#">• Kids &amp; Babies (5)</a></li>
                    <li><a href="#">• Electronics (4)</a></li>
                   </ul>
               </div> 
               <h2 class="accordion-header inactive-header">Home &amp; Garden (8)</h2> 
               <div class="accordion-content"> 
                   <ul>
                    <li><a href="#">• Jackets (7)</a></li>
                    <li><a href="#">• Kids &amp; Babies (5)</a></li>
                    <li><a href="#">• Electronics (4)</a></li>
                   </ul>
               </div> 
          </div>
        </div>
        <!-- End Accordion -->


        <div class="price-filter mb30">
            <h3>Price Filter</h3>
          <div class="price-inner clearfix">
            <div id="slider" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>
            <p class="low-price">$0</p>
            <p class="high-price">$1000</p>
            <div class="clear"></div>
          </div>
        </div>

        <div class="feat-product mb30">
          <h3>Popular Products</h3>
          <div class="feat-boxes2">
            <a href="#"><img src="images/featboxes1.png" alt=""></a>
            <div class="feat-right2">
              <a href="#">Iphone 5 Black</a>
              <span>$475.00</span>
            </div>
            <div class="clear"></div>
          </div>
          <div class="feat-boxes2">
            <a href="#"><img src="images/featboxes2.png" alt=""></a>
            <div class="feat-right2">
              <a href="#">Iphone 4 White</a>
              <span>$375.00</span>
            </div>
            <div class="clear"></div>
          </div>

          <div class="feat-boxes2">
            <a href="#"><img src="images/featboxes3.png" alt=""></a>
            <div class="feat-right2">
              <a href="#">Samasung Galaxy note 3</a>
              <span>$475.00</span>
            </div>
            <div class="clear"></div>
          </div>
          <div class="feat-boxes2">
            <a href="#"><img src="images/featboxes4.png" alt=""></a>
            <div class="feat-right2">
              <a href="#">Mac X33 Laptop</a>
              <span>$75.00</span>
            </div>
            <div class="clear"></div>
          </div>
        </div>

      
      </div>
      <!-- End Home Blog -->   

      <div class="clear"></div>

  </div>

@stop